window.DEBUG = true;

//add functions in jQuery for work with SVG nodes classes
(function(){
	/*
	 * .addClassSVG(className)
	 * Adds the specified class(es) to each of the set of matched SVG elements.
	 */
	$.fn.addClassSVG = function(className){
		$(this).attr('class', function(index, existingClassNames) {
			return existingClassNames + ' ' + className;
		});
		return this;
	};

	/*
	 * .removeClassSVG(className)
	 * Removes the specified class to each of the set of matched SVG elements.
	 */
	$.fn.removeClassSVG = function(className){
		$(this).attr('class', function(index, existingClassNames) {
			var re = new RegExp(className, 'g');
			return existingClassNames.replace(re, '');
		});
		return this;
	};
})()

function Orchestra(settings)
{
	var self = this,
		audioManager, instrumentsView, videoManager,
		controlPlay, controlPause, 
		controlVolume, controlReset;

	function _hideWaiter()
	{
		$('.loading').toggle();
		$('.application-container').toggle();
	}

	function _playAll()
	{
		audioManager.playAll();
		videoManager.playAll();
		_setStopButtonActive();
	}

	function _stopAll()
	{
		audioManager.stopAll();
		videoManager.stopAll();
		_setPlayButtonActive();
	}

	function _resetInstruments()
	{
		audioManager.resetActiveInstrument();
		instrumentsView.resetActiveInstrument();
	}

	function _resetMediaPosition() {
		audioManager.resetMediaPosition();
		videoManager.resetMediaPosition();
	}

	function _resetUiState()
	{
		_resetInstruments();
		audioManager.setVolume(controlVolume.value);
	}

	function _reset() 
	{
		_stopAll();
		_resetInstruments();
		_resetMediaPosition();
	}

	function _ready()
	{
		_hideWaiter();
		_resetUiState();
	}

	function _playedInstrumentReseted()
	{
		audioManager.resetActiveInstrument();
		instrumentsView.resetActiveInstrument();
	}
	
	function _playedInstrumentChanged(instrumentName)
	{
		audioManager.setInstrument(instrumentName);
	}

	function _updateActiveInstuments(instrumentsForActivate, instrumentsForDeactivate)
	{
		instrumentsView.updateActiveInstuments(instrumentsForActivate, instrumentsForDeactivate);
	}

	function _setStopButtonActive()
	{
		$(controlPlay).addClass("active");
		$(controlPause).removeClass("active");
	}

	function _setPlayButtonActive()
	{
		$(controlPlay).removeClass("active");
		$(controlPause).addClass("active");
	}

	function _onVolumeRangeChange() { 
		audioManager.setVolume(parseFloat(this.value));
	}

	function _initControls() {
		controlPlay = document.querySelector(settings.Controls.Play),
		controlPause = document.querySelector(settings.Controls.Pause),
		controlVolume = document.querySelector(settings.Controls.Volume),
		controlReset = document.querySelector(settings.Controls.Reset);

		controlPlay.addEventListener('click', function() { 
			_playAll();
		});

		controlPause.addEventListener('click', function() { 
			_stopAll();		
		});

		controlVolume.addEventListener('change', _onVolumeRangeChange);
		controlVolume.addEventListener('input', _onVolumeRangeChange);

		controlReset.addEventListener('click', function() { 
			controlVolume.value = 0.5;
			_resetUiState();
		});

		_setPlayButtonActive();
	}

	function _getSyncCurrentTime()
	{
		if ($.browser.mobile)
		{
			return audioManager.getCurrentTimeOfTrack();
		}
		else
		{
			return videoManager.getSyncVideo().currentTime;
		}
	}

	function _startLoad()
	{
		var urls = [], 
			progressCounter, blobLoader;

		blobLoader = new BlobLoader({
			Urls: urls.concat(videoManager.getFilesUrls(), audioManager.getFilesUrls()),
		});

		blobLoader.load(function(blobsMap) {
			audioManager.init(blobsMap);
			videoManager.init(blobsMap);
		})

		progressCounter = new ProgressCounter({
			CountersInfo: [
				{ Counter: blobLoader, Coefficient: 0.75 },
				{ Counter: videoManager, Coefficient: 0.14 },
				{ Counter: audioManager, Coefficient: 0.11 }
			],
			ProgressCounted: function(percent)
			{
				settings.ProgressNode.innerHTML = percent + "%";
				if (percent === 100)
				{
					setTimeout(_ready, 200);
				}
			}
		});

		progressCounter.startCount();
	}

	return {
		init: function() {
			if (!$.browser.mobile)
				$(document.body).addClass('Desktop');

			videoManager = new VideoManager({
				Videos: settings.Videos,
				Quality: settings.Quality
			});

			audioManager = new SingleTrackAudioManager({
				ActiveInstrumentsChanged: _updateActiveInstuments,
				GetSyncCurrentTimeFunc: _getSyncCurrentTime,
				Instruments: settings.Instruments,
				Quality: settings.Quality,
				MasterTrackSeconds: settings.MasterTrackSeconds,
				TrackEnded: _reset
			});

			instrumentsView = new InstrumentsView({
				PlayedInstrumentChanged: _playedInstrumentChanged,
				PlayedInstrumentReseted: _playedInstrumentReseted
			});

			_initControls();
			if (!$.browser.mobile)
				new InstrumentTooltip().init();

			_startLoad();
		}
	}
}

function ProgressCounter(settings)
{
	function _getProgressPercent()
	{
		var countersInfo = settings.CountersInfo,
			isAllLoaded = true,
			allProgress = 0, 
			readyPercent, oneProgress, counter, coef, i;

		for (i = 0; i < countersInfo.length; i++)
		{
			counter = countersInfo[i].Counter;
			coef = countersInfo[i].Coefficient;

			readyPercent = counter.getReadyPercent();
			if (isAllLoaded)
				isAllLoaded = readyPercent === 100;

			oneProgress = readyPercent * coef;
			allProgress += oneProgress;
		}

		return isAllLoaded ? 100 : Math.floor(allProgress);
	}

	function _nextCount()
	{
		setTimeout(function(){
			var percent = _getProgressPercent();
			settings.ProgressCounted(percent);

			if (percent !== 100)
				_nextCount();
		}, 100);
	}

	return {
		startCount: _nextCount
	}
}

function BlobLoader(settings)
{
	var blobLinks = {},
		loadCounter = 0,
		downloadInfo = {},
		loadedPercent = 0;

	function _updateLoadPercent()
	{
		var allTotal = 0, 
			allLoaded = 0,
			url;

		for (url in downloadInfo)
		{
			allTotal += downloadInfo[url].Total;
			allLoaded += downloadInfo[url].Loaded;
		}

		loadedPercent = Math.floor(allLoaded * 100 / allTotal);
	}

	function _updateDownloadInfo(url, event)
	{
		if (!downloadInfo[url])
		{
			downloadInfo[url] = {
				Total: event.total
			}
		}
		downloadInfo[url].Loaded = event.loaded;

		if (Object.keys(downloadInfo).length === settings.Urls.length)
		{
			_updateLoadPercent();
		} 
	}

	function _loadOneUrl(sourceURL, callback)
	{
		var xhr = new XMLHttpRequest();

		xhr.addEventListener("progress", function(event) {
			_updateDownloadInfo(sourceURL, event);
		});

		xhr.addEventListener('readystatechange', function() {
			var URLUtil = window.URL || window.webkitURL,
				blobLink;

			if (this.readyState == 4 && this.status == 200){
				blobLink = URLUtil.createObjectURL(this.response);
				blobLinks[sourceURL] = blobLink;

				callback();
			}
		})

		xhr.open('GET', sourceURL);
		xhr.setRequestHeader("Cache-Control", "max-age=315360000");
		xhr.responseType = 'blob';

		xhr.send();      
	}

	return {
		getReadyPercent: function()
		{
			return loadedPercent;
		},
		load: function(callback)
		{
			settings.Urls.forEach(function(url){
				_loadOneUrl(url, function(){
					loadCounter++;
					if (settings.Urls.length === loadCounter)
					{
						loadedPercent = 100;
						callback(blobLinks);
					}
				});
			});
		}
	}
}

function VideoManager(settings)
{
	var self = this,
		folders = {
			low: 'video/low/',
			medium: 'video/med/',
			hi: 'video/hi/'
		},
		isVideoEnabled = settings.EnableVideoInMobile || !$.browser.mobile,
		videos = [],
		readyPercent = 0,
		fakePercent = 0,
		isInited = false,
		intervalMakeFakePercent;

	function _GetVideoSrc(videoDescr)
	{
		return folders[settings.Quality] + videoDescr.Video;
	}

	function _onVideoCanPlayed()
	{
		this.removeEventListener('canplay', _onVideoCanPlayed);
		this.isCanPlayed = true;
	}

	function _checkOneVideoComplete(video)
	{
		if ($.browser.mozilla)
		{
			return video.readyState === video.HAVE_ENOUGH_DATA || video.isCanPlayed;
		}
		else
		{
			return video.isCanPlayed;
		}
	}

	function _getFakePercent(realEp)
	{
		if (intervalMakeFakePercent == null && isInited)
			_startMakeFakePercent();

		return readyPercent + fakePercent >= 100 ? fakePercent - readyPercent : fakePercent;
	}

	function _startMakeFakePercent()
	{
		intervalMakeFakePercent = setInterval(function(){
			if (fakePercent < 99)
			{
				fakePercent += 1;
			}
		}, 200);
	}

	function _stopMakeFakePercent()
	{
		clearInterval(intervalMakeFakePercent);
		intervalMakeFakePercent = null;
	}

	function _addVideo(videoDescr, blobLinks)
	{
		var video = document.createElement('video'),
			source = document.createElement('source'),
			isIos = $.browser.ipad || $.browser.iphone,
			realURL = _GetVideoSrc(videoDescr),
			blobURL = blobLinks[realURL];

		videos.push(video);
		if (!isIos)
		{
			video.addEventListener('canplay', _onVideoCanPlayed);
		}
		video.preload = "auto";

		source.src = blobURL;
		source.type = videoDescr.Type;
		video.appendChild(source);

		videoDescr.Container.appendChild(video);

		if (isIos)
		{
			_onVideoCanPlayed.apply(video);
		}

		return video;
	}

	return {
		getReadyPercent: function()
		{
			var readyVideosCount = 0,
				result;

			if (!isVideoEnabled)
			{
				readyPercent = 100;
			}
			else
			{
				videos.forEach(function(video){
					if (_checkOneVideoComplete(video))
						readyVideosCount++;
				});

				readyPercent = readyVideosCount * 100 / settings.Videos.length;
			}

			if (readyPercent !== 100)
			{
				result = _getFakePercent() + readyPercent;
			}
			else
			{
				result = 100;
				_stopMakeFakePercent();
			}

			return result;
		},
		init: function(blobLinks)
		{
			if (isVideoEnabled)
			{
				settings.Videos.forEach(function(videoDescr){
					_addVideo(videoDescr, blobLinks);
				});
			}
			else
			{
				settings.Videos.forEach(function(videoDescr){ 
					if (videoDescr.Container)
						$(videoDescr.Container).hide();
				});
			}
			isInited = true;
		},
		getFilesUrls: function (videoDescr)
		{
			var result;
			if (isVideoEnabled)
			{
				result = settings.Videos.map(function(videoDescr)
				{
					return _GetVideoSrc(videoDescr);
				});
			}

			return result || [];
		},
		playAll: function()
		{
			videos.forEach(function(video)
			{
				video.play();
			});
		},
		stopAll: function()
		{
			videos.forEach(function(video)
			{
				video.pause();
			});
		},
		getSyncVideo: function()
		{
			return videos[0];
		},
		resetMediaPosition: function() {
			videos.forEach(function(video)
			{
				video.currentTime = 0;
			});
		}
	}
}

function InstrumentsView(settings)
{
	var self = this,
		playedInstrument = null;

	function _init()
	{
		var svg = document.querySelectorAll('svg g');
		[].forEach.call(svg, function(gElement){
			var instrumentName = window.Utils.getDataAttr(gElement, 'instrument-name');
			if (!instrumentName) 
				return;

			gElement.addEventListener('click', function(event){
				if (!instrumentName)
					return;

				if (playedInstrument && playedInstrument === gElement)
				{
					settings.PlayedInstrumentReseted();
				}
				else
				{
					if (playedInstrument)
						$(playedInstrument).removeClassSVG('playedNow')
					playedInstrument = gElement;
					$(playedInstrument).addClassSVG('playedNow')

					settings.PlayedInstrumentChanged(instrumentName);
				}
			});
		})
	}

	function _getAllInstruments()
	{
		return [].slice.call(document.querySelectorAll('svg g.instrument'));
	}

	function _getInstrument(name, all)
	{
		var instruments = _getAllInstruments(),
			i, currInstrument, result;

		for (i = 0; i < instruments.length; i++)
		{
			currInstrument = instruments[i];
			if (window.Utils.getDataAttr(currInstrument, 'instrument-name') === name)
			{
				if (all)
				{
					if (!result)
						result = [];
					result.push(currInstrument);
				}
				else
				{
					result = currInstrument;
					break;
				}
			}
		}

		return result;
	}

	function _updateActiveInstuments(instrumentsForActivate, instrumentsForDeactivate)
	{
		var i, j, currInstrumentName,
			instrumentGroups, currInstrGroup, currInstrument;

		for (i = 0; i < instrumentsForDeactivate.length; i++)
		{
			currInstrumentName = instrumentsForDeactivate[i];
			instrumentGroups = _getInstrument(currInstrumentName, true);
			if (instrumentGroups)
			{
				instrumentGroups = Array.isArray(instrumentGroups) ? instrumentGroups : [instrumentGroups];
				for (j = 0; j < instrumentGroups.length; j++)
				{
					currInstrGroup = instrumentGroups[j];
					currInstrument = currInstrGroup.querySelector("path");
					currInstrument.removeAttribute("stroke");
					currInstrument.removeAttribute("stroke-width");
				}
			}
		}

		for (i = 0; i < instrumentsForActivate.length; i++)
		{
			currInstrumentName = instrumentsForActivate[i];
			instrumentGroups = _getInstrument(currInstrumentName, true);
			if (instrumentGroups)
			{
				instrumentGroups = Array.isArray(instrumentGroups) ? instrumentGroups : [instrumentGroups];
				for (j = 0; j < instrumentGroups.length; j++)
				{
					currInstrGroup = instrumentGroups[j];
					currInstrument = currInstrGroup.querySelector("path");
					currInstrument.setAttribute("stroke", "red");
					currInstrument.setAttribute("stroke-width", "3");
				}
			}
		}
	}

	_init();

	return {
		getInstrument: _getInstrument,
		resetActiveInstrument: function()
		{
			if (playedInstrument)
			{
				$(playedInstrument).removeClassSVG('playedNow')
				playedInstrument = null;
			}
		},
		updateActiveInstuments: _updateActiveInstuments
	}
}

function SingleTrackAudioManager(settings)
{
	var self = this,
		folders = {
			low: 'music/low/',
			medium: 'music/med/',
			hi: 'music/hi/'
		},
		mergetTrackSrc = folders[settings.Quality] + 'merged_track.mp3',
		masterName = 'master',
		isPlayed = false,
		playedTime = 0,
		orders = {},
		fakePercent = 0,
		isInited = false,
		playedInstrument = masterName, 
		audio,
		taimingObj,
		lastActiveInstuments, intervalMakeFakePercent;


	function _getFakePercent()
	{
		if (intervalMakeFakePercent == null && isInited)
			_startMakeFakePercent();

		return fakePercent;
	}

	function _startMakeFakePercent()
	{
		intervalMakeFakePercent = setInterval(function(){
			if (fakePercent < 99)
			{
				fakePercent += 1;
			}
		}, 200);
	}

	function _stopMakeFakePercent()
	{
		clearInterval(intervalMakeFakePercent);
		intervalMakeFakePercent = null;
	}

	function _getStartTimeByInstrument(name)
	{
		return orders[name] * settings.MasterTrackSeconds;
	}

	function _onAudioCanPlay()
	{
		this.removeEventListener('canplay', _onAudioCanPlay);
		this.isCanPlayed = true;
	}

	function _createAudio(url)
	{
		var audio = document.createElement('audio'),
			isIos = $.browser.ipad || $.browser.iphone;
		
		if (!isIos)
		{
			audio.addEventListener('canplay', _onAudioCanPlay);
		}

		audio.addEventListener('timeupdate', _onTimeUpdate);
		audio.preload = "auto";
		audio.src = url;

		if (window.DEBUG)
			window.audio = audio;

		if (isIos)
		{
			_onAudioCanPlay.apply(audio);
		}

		return audio;
	}

	function _stopAll()
	{
		if (!isPlayed)
			return;

		isPlayed = false;

		audio.pause();
	}

	function _setAudioTrack(instrumentName)
	{
		var time = _getStartTimeByInstrument(instrumentName) + settings.GetSyncCurrentTimeFunc();
		audio.currentTime = time;

		playedInstrument = instrumentName;
	}

	function _getCurrentTimeOfTrack()
	{
		var time = audio.currentTime - _getStartTimeByInstrument(playedInstrument);
		return time;
	}

	function _onTimeUpdate(event)
	{
		if (!isPlayed)
			return;

		var target = event.target || event.srcElement,
			currTime = _getCurrentTimeOfTrack(),
			instrumentsForActivate = [],
			instrumentsForDeactivate = [],
			activeInstruments, i, currInstr;

		if (currTime >= settings.MasterTrackSeconds) 
		{
			if (settings.TrackEnded) 
			{
				settings.TrackEnded();
			}
			activeInstruments = [];
		}
		else 
		{
			activeInstruments = _getActiveInstrNamesByTime(currTime);
		}		

		if (lastActiveInstuments)
		{
			for (i = lastActiveInstuments.length - 1; i >= 0; i--)
			{
				currInstr = lastActiveInstuments[i];
				if (activeInstruments.indexOf(currInstr) === -1)
				{
					lastActiveInstuments.splice(i, 1);
					instrumentsForDeactivate.push(currInstr);
				}
			}
		}

		for (i = 0; i < activeInstruments.length; i++)
		{
			currInstr = activeInstruments[i];
			if (lastActiveInstuments)
			{
				if (lastActiveInstuments.indexOf(currInstr) === -1)
				{
					lastActiveInstuments.push(currInstr);
					instrumentsForActivate.push(currInstr);
				}
			}
			else
			{
				lastActiveInstuments = [];
				lastActiveInstuments.push(currInstr);
				instrumentsForActivate.push(currInstr);
			}
		}

		if (instrumentsForActivate.length > 0 || instrumentsForDeactivate.length > 0)
		{
			settings.ActiveInstrumentsChanged(instrumentsForActivate, instrumentsForDeactivate);
		}
	}

	function _getActiveInstrNamesByTime(currTime)
	{
		var result = [],
			instrName, i, curTiming, curInterval, taimObj;

		taimObj = _getTaimingObj();

		for (instrName in taimObj)
		{
			curTiming = taimObj[instrName];
			for (i = 0; i < curTiming.length; i++)
			{
				curInterval = curTiming[i];
				if (currTime >= curInterval[0] && currTime <= curInterval[1])
				{
					result.push(instrName);
					break;
				}
			}
		}

		return result;
	}

	function _getTaimingObj()
	{
		if (!taimingObj)
		{
			taimingObj = {
				viulu: [[71, 107], [116, 124], [141, 166], [180, 200], [210, 415], [421, 482]],
				alttoviulu: [[71, 107], [116, 124], [141, 166], [180, 200], [210, 482]],
				sello: [[71, 107], [116, 124], [141, 200], [204, 309], [363, 482]],
				kontrabasso: [[0, 12], [38, 58], [78, 90], [97, 107], [118, 125], [141, 200], [204, 310], [360, 482]],
				oboe: [[58, 70], [101, 106], [118, 166], [180, 197], [210, 223], [227, 297], [310, 362], [399, 416], [421, 482]],
				klarinetti: [[58, 70], [101, 166], [180, 197], [210, 297], [310, 415], [421, 482]],
				fagotti: [[0, 11], [58, 70], [81, 310], [345, 482]],
				huilu: [[58, 70], [118, 122], [137, 166], [193, 202], [210, 297], [310, 362], [421, 482]],
				trumpetti: [[28, 58], [146, 171], [186, 187], [193, 194], [197, 201], [210, 212], [217, 218], [230, 242], [249, 250], [254, 255], [260, 262], [274, 285], [292, 293], [301, 310], [418, 419], [422, 431], [437, 482]],
				pasuuna: [[0, 24], [28, 58], [166, 171], [186, 187], [193, 201], [210, 212], [217, 218], [230, 262], [274, 293], [301, 310], [418, 482]],
				kauratorvi: [[0, 23], [28, 58], [81, 84], [92, 104], [118, 166], [180, 201], [212, 223], [230, 267], [273, 310], [401, 408], [418, 482]],
				tuuba: [[0, 11], [28, 57], [118, 223], [230, 267], [274, 310], [416, 482]],
				lyomasoittimet: [[210, 230], [253, 274], [424, 427], [434, 454]],
				patarummut: [[19, 41], [66, 71], [101, 104], [118, 121], [139, 144], [150, 153], [158, 162], [166, 310], [360, 482]]
			};
		}

		return taimingObj;
	}

	return {
		getReadyPercent: function()
		{
			if (audio && audio.isCanPlayed)
			{
				_stopMakeFakePercent();
				result = 100;
			}
			else
			{
				result = _getFakePercent();
			}
			return result;
		},
		init: function (blobLinks)
		{
			var url = blobLinks[mergetTrackSrc];

			settings.Instruments.forEach(function(instrument){
				orders[instrument.Name] = instrument.AudioOrder;
			});
			orders[masterName] = 0;

			audio = _createAudio(url);
			isInited = true;
		},
		getFilesUrls: function ()
		{
			return mergetTrackSrc;
		},
		setInstrument: function(name)
		{
			_setAudioTrack(name);
		},
		resetActiveInstrument: function()
		{
			_setAudioTrack(masterName);
		},
		playAll: function()
		{
			if (isPlayed)
				return;

			isPlayed = true;
			//sync time between audio and control
			_setAudioTrack(playedInstrument);

			audio.play();
		},
		stopAll: _stopAll,
		resetMediaPosition: function() {
			audio.currentTime = 0;
		},
		setVolume: function(value)
		{
			audio.volume = value;
		},
		getCurrentTimeOfTrack: _getCurrentTimeOfTrack
	}
}

function InstrumentTooltip() {
	var loadedImagesCount, 
		tooltipsCount, 
		tooltipNode, 
		imagesCache = {};

	function _onMouseOver(event) {
		var $this = $(this),
			rect = this.getBoundingClientRect(),
			src = $this.data('tooltip-src'),
			positionInfo = $this.data('tooltip-position'),
			position = 'left', offsetLeft = 0, offsetTop = 0,
			tooltipWidth, tooltipHeight,
			newTop, newLeft;

		$(tooltipNode).hide();

		if (positionInfo) {
			positionInfo = positionInfo.split(';');
			position = positionInfo[0];
			offsetLeft = parseInt(positionInfo[1] || '0', 10);
			offsetTop = parseInt(positionInfo[2] || '0', 10);
		}

		tooltipNode.src = src;
		if (!imagesCache[tooltipNode.src]) {
			return;
		}
		tooltipWidth = imagesCache[tooltipNode.src].Width;
		tooltipHeight = imagesCache[tooltipNode.src].Height;

		newLeft = rect.left + $(window).scrollLeft() + offsetLeft;
		newTop = rect.top + $(window).scrollTop() + offsetTop;

		switch (position) {
			case 'left':
				newTop += (rect.height - tooltipHeight) / 2;
				newLeft -= tooltipWidth;
				break;

			case 'right':
				newTop += (rect.height - tooltipHeight) / 2;
				newLeft += rect.width;
				break;

			case 'top':
				newTop -= tooltipHeight;
				newLeft += (rect.width - tooltipWidth) / 2;
				break;

			case 'bottom':
				newTop += rect.height;
				newLeft += (rect.width - tooltipWidth) / 2;
				break;
		}

		tooltipNode.style.top = Math.round(newTop) + 'px';
		tooltipNode.style.left = Math.round(newLeft) + 'px';
		$(tooltipNode).fadeIn();
	};

	function _onMouseOut(event) {
		$(tooltipNode).fadeOut();
	};

	function _onImageLoad() {
		loadedImagesCount++;
		imagesCache[this.src] = { Width: this.width, Height: this.height };
		if (tooltipsCount === loadedImagesCount) {
			// fire smth
		}
	}

	function _preloadImages() {
		var groups = document.querySelectorAll('svg g.tooltip'), 
			src, image,
			i;

		loadedImagesCount = 0;
		tooltipsCount = groups.length;
		for (i = 0; i < groups.length; i++) {
			src = $(groups[i]).data('tooltip-src');
			image = new Image();
			image.onload = _onImageLoad;
			image.src = src;
		}
	}

	function _init() {
		$('svg g.tooltip').hoverIntent({
			over: _onMouseOver, 
			out: _onMouseOut,
			timeout: 50
		});

		tooltipNode = document.createElement('img');
		tooltipNode.className = 'tooltip-image';
		tooltipNode.style.display = 'none';

		document.body.appendChild(tooltipNode);

		_preloadImages();
	}

	return {
		init: _init
	};
}

window.Utils = {
	getDataAttr: function(element, attributeName)
	{
		return element.getAttribute('data-' + attributeName);
	},
	setDataAttr: function(element, attributeName, value)
	{
		return element.setAttribute('data-' + attributeName, value);
	}
}